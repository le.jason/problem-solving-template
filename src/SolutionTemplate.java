import java.io.*;

class SolutionTemplate
{
    public static void main(String[] args)
    {
        // Please take a look of how data is suppose to be constructed in readme
        File file = new File("assets//data.txt");

        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            int numLines = Integer.parseInt(br.readLine());
            System.out.println("Number of lines: " + numLines);
            for(int i = 0; i < numLines; i++) {
                // problem solving logic goes here:
                System.out.println("Line: " + br.readLine());

            }

            fr.close();
        } catch (FileNotFoundException e) {
            System.out.println("Incorrect file path");
        } catch (IOException e) {
            System.out.println("File was not formatted correctly");
        }
    }
}