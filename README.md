This is a Java Problem solving template I use for debugging when solving Java
Hackerrank/ Leet code problems.

The data template can be found in the assets directory and must follow the following
format:

// lines of data
// data
// data
// data

e.g. 
2
hello
world

